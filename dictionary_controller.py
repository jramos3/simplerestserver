import json
import cherrypy

class DictionaryController(object):
    def __init__(self):
        self.myd = dict()

    def get_value(self, key):
        value = self.myd[key]

    def GET_KEY(self, key):
        o = {'result': 'success'}
        key = str(key)
        try:
            value = self.get_value(key)
            if value is not None:
                o['key'] = key
                o['value'] = value
            else:
                o['result'] = 'error'
                o['message'] = 'None type value associated with requested key'
        except KeyError as e:
            o['result'] = 'error'
            o['message'] = 'key not found'
        except Exception as e:
            o['result'] = 'error'
            o['message'] = str(e)

        return json.dumps(o)
   

    def PUT_KEY(self, key):
        o = {'result':'success'}
        key = str(key)
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))
        print(data)

        try:
            val = data['value']
            self.myd[key] = val 
        except Exception as e:
            o['result'] = 'error'
            o['message'] = str(e)

        return json.dumps(o)

    def DELETE_KEY(self, key):
        o = {'result':'success'}
        key = str(key)
        
        try:
            del self.myd[key]
        except KeyError as e:
            o['result'] = 'error'
            o['message'] = 'key not found'
        except Exception as e:
            o['result'] = 'error'
            o['message'] = str(e)
        return json.dumps(o)

    def POST_INDEX(self):
        o = {'result':'success'}
        data = cherrypy.request.body.read()
        data = json.loads(data)

        try:
            key = data['key']
            val = data['value']
            self.myd[key] = val
        except Exception as e:
            o['result'] = 'error'
            o['message'] = str(e)

        return json.dumps(o)

    def GET_INDEX(self):
        o = {'result': 'success'}
        o['entries'] = []

        try:
            temp = []

            for key, value in self.myd.items():
                temp.append({'key': key, 'value': value})
            
            o = {'result': 'success', 'entries': temp}
        except KeyError as e:
            o['result'] = 'error'
            o['message'] = str(e)

        return json.dumps(o)

    def DELETE_INDEX(self):
        o = {'result':'success'}

        try:
            for key, _ in list(self.myd.items()):
                del self.myd[key] # self.myd.clear()
            o['result'] = 'success'
        except Exception as e:
            o['result'] = 'error'
            o['message'] = str(e)
        
        return json.dumps(o)